cwlVersion: v1.0
class: CommandLineTool
requirements:
  - class: InlineJavascriptRequirement
  - class: InitialWorkDirRequirement
    listing:
      - $(inputs.index[0])
      - $(inputs.index[1])
      - $(inputs.index[2])
      - $(inputs.index[3])
      - $(inputs.index[4])
      - $(inputs.index[5])
inputs:
  index:
    type:
      type: array
      items: File
      inputBinding:
        valueFrom: |
          ${
            if (self.basename == inputs.index[0].basename) {
              var split = self.basename.split('.');
              return split.slice(0, split.length - 2).join('.');
            } else {
              return null;
            }
          }
    format: http://edamontology.org/format_3326 # index
    inputBinding:
      prefix: -x
  read:
    type: File
    format: http://edamontology.org/format_1930 # FASTQ
    inputBinding:
      prefix: -U
  output_file:
      type: string?
      inputBinding:
        prefix: -S
stdout: |
  ${
    if (!inputs.output_file) {
      var split = inputs.index[0].basename.split('.')
      return "bowtie2_".concat(split.slice(0, split.length - 2).join('.').concat(".bam"));
    } else {
      return inputs.output_file;
    }
  }
outputs:
  alignment:
    type: File
    format: http://edamontology.org/format_2572 # BAM
    outputBinding:
      glob: |
        ${
          if (!inputs.output_file) {
            var split = inputs.index[0].basename.split('.')
            return "bowtie2_".concat(split.slice(0, split.length - 2).join('.').concat(".bam"));;
          } else {
            return inputs.output_file;
          }
        }
  
baseCommand: ["bowtie2"]