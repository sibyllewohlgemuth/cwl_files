cwlVersion: v1.0
class: CommandLineTool
requirements:
  - class: InlineJavascriptRequirement
inputs:
  reference:
    type: File
    format: http://edamontology.org/format_1929 # FASTA
    inputBinding:
      position: 1
  output_prefix:
    type: string?
    default: default
    inputBinding:
      position: 2
      prefix: -o
      valueFrom: |
        ${
          if (self == "default") {
            return inputs.reference.nameroot;
          } else {
            return self;
          }
        }
    doc: |
      Specify a filename prefix for the reference genome index. Default: use the filename prefix of the reference
  tmp_dir:
    type: string?
    inputBinding:
      position: 2
      prefix: -td
    doc: |
      Specify a temporary directory where to construct the index. Default: use the output directory.
outputs:
  index:
    type: {type: array, items: File}
    format: http://edamontology.org/format_3326 # index
    outputBinding:
      glob:
      - |
        ${
          if (inputs.output_prefix == "default") {
            return inputs.reference.nameroot + ".lf.drp"
          } else {
            return inputs.output_prefix + ".lf.drp"
          }
        }
      - |
        ${
          if (inputs.output_prefix == "default") {
            return inputs.reference.nameroot + ".lf.drs"
          } else {
            return inputs.output_prefix + ".lf.drs"
          }
        }
      - |
        ${
          if (inputs.output_prefix == "default") {
            return inputs.reference.nameroot + ".lf.drv"
          } else {
            return inputs.output_prefix + ".lf.drv"
          }
        }
      - |
        ${
          if (inputs.output_prefix == "default") {
            return inputs.reference.nameroot + ".lf.pst"
          } else {
            return inputs.output_prefix + ".lf.pst"
          }
        }
      - |
        ${
          if (inputs.output_prefix == "default") {
            return inputs.reference.nameroot + ".rid.concat"
          } else {
            return inputs.output_prefix + ".rid.concat"
          }
        }
      - |
        ${
          if (inputs.output_prefix == "default") {
            return inputs.reference.nameroot + ".rid.limits"
          } else {
            return inputs.output_prefix + ".rid.limits"
          }
        }
      - |
        ${
          if (inputs.output_prefix == "default") {
          return inputs.reference.nameroot + ".sa.ind"
          } else {
            return inputs.output_prefix + ".sa.ind"
          }
        }
      - |
        ${
          if (inputs.output_prefix == "default") {
            return inputs.reference.nameroot + ".sa.len"
          } else {
            return inputs.output_prefix + ".sa.len"
          }
        }
      - |
        ${
          if (inputs.output_prefix == "default") {
            return inputs.reference.nameroot + ".sa.val"
          } else {
            return inputs.output_prefix + ".sa.val"
          }
        }
      - |
        ${
          if (inputs.output_prefix == "default") {
            return inputs.reference.nameroot + ".txt.concat"
          } else {
            return inputs.output_prefix + ".txt.concat"
          }
        }
      - |
        ${
          if (inputs.output_prefix == "default") {
            return inputs.reference.nameroot + ".txt.limits"
          } else {
            return inputs.output_prefix + ".txt.limits"
          }
        }
      - |
        ${
          if (inputs.output_prefix == "default") {
            return inputs.reference.nameroot + ".txt.size"
          } else {
            return inputs.output_prefix + ".txt.size"
          }
        }

baseCommand: ["yara_indexer"]

doc: |
  Usage:   yara_indexer [OPTIONS] <REFERENCE FILE>
  
  Options: -o, --output-prefix PREFIX    Specify a filename prefix for the reference genome index. Default: use the filename prefix of the reference
           -td, --tmp-dir STR           Specify a temporary directory where to construct the index. Default: use the output directory.