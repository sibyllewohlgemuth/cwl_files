cwlVersion: v1.0
class: CommandLineTool
requirements:
  - class: InlineJavascriptRequirement
inputs:
  reference:
    type: File
    format: http://edamontology.org/format_1929 # FASTA
    inputBinding:
      position: 1
  output_prefix:
    type: string?
    default: default
    inputBinding:
      position: 2
      prefix: -p
      valueFrom: |
        ${
          if (self == "default") {
            return inputs.reference.nameroot;
          } else {
            return self;
          }
        }
    doc: |
      Specify a filename prefix for the reference genome index. Default: use the filename prefix of the reference
outputs:
  index:
    type: {type: array, items: File}
    format: http://edamontology.org/format_3326 # index
    outputBinding:
      glob:
      - |
        ${
          if (inputs.output_prefix == "default") {
            return inputs.reference.nameroot + ".amb"
          } else {
            return inputs.output_prefix + ".amb"
          }
        }
      - |
        ${
          if (inputs.output_prefix == "default") {
            return inputs.reference.nameroot + ".ann"
          } else {
            return inputs.output_prefix + ".ann"
          }
        }
      - |
        ${
          if (inputs.output_prefix == "default") {
            return inputs.reference.nameroot + ".bwt"
          } else {
            return inputs.output_prefix + ".bwt"
          }
        }
      - |
        ${
          if (inputs.output_prefix == "default") {
            return inputs.reference.nameroot + ".pac"
          } else {
            return inputs.output_prefix + ".pac"
          }
        }
      - |
        ${
          if (inputs.output_prefix == "default") {
            return inputs.reference.nameroot + ".sa"
          } else {
            return inputs.output_prefix + ".sa"
          }
        }

baseCommand: ["bwa", "index"]

doc: |
  Usage:   bwa index [options] <in.fasta>
  
  Options: -a STR    BWT construction algorithm: bwtsw, is or rb2 [auto]
           -p STR    prefix of the index [same as fasta name]
           -b INT    block size for the bwtsw algorithm (effective with -a bwtsw) [10000000]
           -6        index files named as <in.fasta>.64.* instead of <in.fasta>.*
  
  Warning: `-a bwtsw' does not work for short genomes, while `-a is' and
           `-a div' do not work not for long genomes.
