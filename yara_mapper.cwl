cwlVersion: v1.0
class: CommandLineTool
requirements:
  - class: InlineJavascriptRequirement
  - class: InitialWorkDirRequirement
    listing:
      - $(inputs.index[0])
      - $(inputs.index[1])
      - $(inputs.index[2])
      - $(inputs.index[3])
      - $(inputs.index[4])
      - $(inputs.index[5])
      - $(inputs.index[6])
      - $(inputs.index[7])
      - $(inputs.index[8])
      - $(inputs.index[9])
      - $(inputs.index[10])
      - $(inputs.index[11])
inputs:
  index:
    type:
      type: array
      items: File
      inputBinding:
        valueFrom: |
          ${
            if (self.basename == inputs.index[0].basename) {
              var split = self.basename.split('.');
              return split.slice(0, split.length - 2).join('.');
            } else {
              return null;
            }
          }
    format: http://edamontology.org/format_3326 # index
    inputBinding:
      position: 1
  read1:
    type: File
    format: http://edamontology.org/format_1930 # FASTQ
    inputBinding:
      position: 2
  read2:
    type: File?
    format: http://edamontology.org/format_1930 # FASTQ
    inputBinding:
      position: 3
  output_file:
    type: string?
    default: default
    inputBinding:
      prefix: -o
      position: 4
      valueFrom: |
        ${
          if (self == "default") {
            var split = inputs.index[0].basename.split('.');
            return "yara_".concat(split.slice(0, split.length - 2).join('.').concat(".bam"));
          } else {
            return self;
          }
        }
outputs:
  alignment:
    type: File
    format: http://edamontology.org/format_2572 # BAM
    outputBinding:
      glob: |
        ${
          if (inputs.output_file == "default") {
            var split = inputs.index[0].basename.split('.');
            return "yara_".concat(split.slice(0, split.length - 2).join('.').concat(".bam"));
          } else {
            return inputs.output_file;
          }
        }
  
baseCommand: ["yara_mapper"]